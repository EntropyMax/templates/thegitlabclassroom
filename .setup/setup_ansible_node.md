## host node configuration

Ubuntu: edit sudo for no pw prompt `sudo visudo`

OLD FILE VIEW

```sh
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
```

NEW | ADD LINE
note: the line added MUST be after the sudo group, otherwise permissions get overwritten when applying policy to %sudo

```sh
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
gitlab ALL=(ALL) NOPASSWD:ALL
```

### setup ssh by creating key and updateing authorized users

```sh
sudo apt-get update && sudo apt-get -y install ssh
# the -P stands for passphrase to use with "" meaning no passphrase
ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa -q -P ""
# add public keys to ~/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLN8FgGPA5lMtJwqO3nurGfW+fcStiXzFNwNYaUZYnrkz8i7wRK7PW0yBfTWUD9hbZu0ybM0C7AC5JnC6i+b0H9aUMgOrBGRrv7tvJnNN/NThoFNPmZc0EBW/S5zTuJPQD2tPpnsbBJrdhpdBahJRf+TBFpkmTxIuGi+jLeUHGYu8MVjdZal4Pe1QAQTHB75m4mr2zBCBtiaKPi/UwzTofew/ha6AQCLHC2MWk/DF4WDqU5XOaNoNZcMMdmY3uCCFAVTApFdGlsVYDerbO9lzw+BQFUCgJSgnApA9V6nJJXln2PSGFg1f1l58sQ4KdX4Gw3zWnO9YYQbAbRmTAERqZt8zySXpBO4BXDTdRGPTQT1Xsj4r+5Ga8cdRQoqw+eALPycuyjnJF0NOhFeKQ4Umcugr5LZLSZL8QgQ5v1OoE2h2TUliwu1noY/gKUbz0vTyiLAkCoZsm86bMtVF1tjT13m1Q26hdvycUSe+ujieXfddGXgvyxbs5T8osk93aoaU= fullo@DESKTOP-PGDNVDV" > ~/.ssh/authorized_keys
sudo chmod 600 ~/.ssh/authorized_keys
```

### Remove the ability to login to system with username/password

Note: when doing this make sure you can login to the system with a private key you have access to.
- The first command will remove the comment and change the "yes" to "no" if the line exists
- The 2nd command will check to see if PasswordAuthentication already exists, if it doesn't it will add one to the end of the file.

```sh
sudo sed -E -i 's|^#?(PasswordAuthentication)\s.*|\1 no|' /etc/ssh/sshd_config
if ! grep '^PasswordAuthentication\s' /etc/ssh/sshd_config; then echo 'PasswordAuthentication no' |sudo tee -a /etc/ssh/sshd_config; fi
```
