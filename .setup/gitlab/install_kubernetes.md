# Installing things in Kubernetes

## Install Windows

- Docker Desktop
- Chocolatay
- Helm
  - `choco install helm`

## Ubuntu

- docker (from docker.io)
- kublet
- Helm

```sh
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```

### Helm from source

```sh
git clone https://github.com/helm/helm.git
cd helm
make
```

## Gitlab ce


```sh
helm install gitlab gitlab/gitlab --set global.hosts.domain=pendancer.com --set certmanager-issuer.email=fullofentropy@gmail.com
```

### Resources

- [Helm Install](https://helm.sh/docs/intro/install/)