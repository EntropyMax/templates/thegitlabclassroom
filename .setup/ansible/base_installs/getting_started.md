# Getting Started

## host configuration

Ubuntu: edit sudo for no pw prompt `sudo visudo`  (if using server, editor has to be setup)
```sh
sudo apt update
sudo apt upgrade -y
sudo apt install nano
export EDITOR=nano
sudo visudo
```

OLD

```sh
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
```

NEW
note: the line added MUST be after the sudo group, otherwise permissions get overwritten when applying policy to %sudo

```sh
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
gitlab ALL=(ALL) NOPASSWD:ALL
```

Ubuntu: update hostname  

```sh
sudo hostnamectl set-hostname <new host name>
sudo nano /etc/hosts
// make manual entry to update hostname
hostnamectl
// verify output displays new hostname
```

## Sample Galexy import

```yml
ansible-galaxy install geerlingguy.gitlab
```

## Sample playbook using general role with var

Note: need to create a vars folder and add vars into main.ymlthe point of the vars.yml is to add/override specific variabls required for the configuration to be unique for the system being configured

the_playbook.yml

```yml
- hosts: servers
  vars_files:
    - vars/main.yml
  roles:
    - { role: geerlingguy.gitlab }
```

vars/main.yml

```yml
gitlab_domain: pendancer.com
gitlab_self_signed_cert_subj: "/C=US/ST=Texas/L=San antonio/O=IT/CN={{ gitlab_domain }}"
```

## Playbook execution

```sh
ansible-playbook 
```

## List hosts the playbook will declare configurations

```sh
ansible-playbook the_playbook.yml --limit ansiblenode1 --list-hosts
```
