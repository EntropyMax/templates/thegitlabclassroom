# ref

TODO
- completion and testing of container setup to be able to act as a microservice
- source files:
  - ssh private key location for use in container
  - hosts file that can be used to determine host to apply changes to
  - take in and take action on ansible actions by providing a command

```sh
docker run -v /c/Users/fullo/.ssh/id_ed25519_2:/ansible/the_ed25519_key:ro -v --rm -it ansible
```
