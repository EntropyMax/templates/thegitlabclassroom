#!/bin/bash
cat the_rsa_key > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
cat the_ed25519_key > ~/.ssh/id_ed25519
chmod 600 ~/.ssh/id_ed25519
"$@" 
# the "$@" allows for the Dockerfile CMD command to work or whatever is input by the user